import os, sys 
import csv, json
import pandas as pd
from pandas import DataFrame
import argparse
from datetime import datetime

# path
parser = argparse.ArgumentParser()
parser.add_argument('--p1', help= 'xlsx')
parser.add_argument('--p2', help= 'merged.csv')
parser.add_argument('--p3', help= 'json')
args = parser.parse_args()

csv_path = args.p1
mcsv_path = args.p2
json_path = args.p3

# read csv file in xlsx
timestep = pd.read_excel(csv_path, 'timestep')
status = pd.read_excel(csv_path, 'status')
text = pd.read_excel(csv_path, 'text')


# timestep
timestep.fillna(method='ffill', inplace=True)   # fill Nan 
timestep_short = timestep.groupby(['Link'], as_index=False)[['name','ID_obj','Type','Moving','InPath','Distance','time_stamp']].agg(lambda x: list(x))   # group to list

# check
#print(timestep_short.head())
#print(len(timestep_short.index), len(status.index), len(text.index))
#print(timestep.columns)
#print(status.columns)
#print(text.columns)

# merge all csvs
merged = pd.merge(text, status, on=['Link'])
merged = pd.merge(merged, timestep_short, on=['Link'])
merged.to_csv(mcsv_path, index=False)
# print(len(merged.index), len(merged.columns))
# print(merged.columns)


# define json 

#attribute={"name":"object_name","ID_obj":"object_ID","Type":"object_type","Moving":"object_moving","InPath":"object_inpath","Distance":"object_distance","time_stamp":"object_timestamp","WindSpeed (m/s)":"wind_speed","DroneSpeed (m/s)":"drone_speed","PilotExperienced":"pilot_experienced","Altitude (m)":"altitude","Temperature (celcius)":"temperature","Distance_from_remote_control (m)":"distance_from_remote_control","Battery_level":"battery_level","Low_visibility":"low_visibility","Normal_frame":"normal_frame","weather":"weather","upside_down":"upside_down","good_motor_condition":"good_motor_condition","going_backwards":"going_backwards","indoor":"indoor","waterproof drone?":"waterproof_drone","flying over":"flying_over","criticality":"criticality","Text1":"text1","Text2":"text2"}
#attribute={"name":["name","object_name"],"ID_obj":["ID_obj","object_ID"],"Type":["Type","object_type"],"Moving":["Moving","object_moving"],"InPath":["InPath","object_inpath"],"Distance":["Distance","object_distance"],"time_stamp":["time_stamp","object_timestamp"],"WindSpeed":["WindSpeed (m/s)","wind_speed"],"DroneSpeed":["DroneSpeed (m/s)","drone_speed"],"PilotExperienced":["PilotExperienced","pilot_experienced"],"Altitude":["Altitude (m)","altitude"],"Temperature":["Temperature (celcius)","temperature"],"Distance_from_remote_control":["Distance_from_remote_control (m)","distance_from_remote_control"],"Battery_level":["Battery_level","battery_level"],"Low_visibility":["Low_visibility","low_visibility"],"Normal_frame":["Normal_frame","normal_frame"],"weather":["weather","weather"],"upside_down":["upside_down","upside_down"],"good_motor_condition":["good_motor_condition","good_motor_condition"],"going_backwards":["going_backwards","going_backwards"],"indoor":["indoor","indoor"],"waterproof":["waterproof drone?","waterproof_drone"],"flying":["flying over","flying_over"],"criticality":["criticality","criticality"],"Text1":["Text1","text1"],"Text2":["Text2","text2"]}
attribute={"name:":["name","object_name"],"ID_obj:":["ID_obj","object_ID"],"Type:":["Type","object_type"],"Moving:":["Moving","object_moving"],"InPath:":["InPath","object_inpath"],"Distance:":["Distance","object_distance"],"time_stamp:":["time_stamp","object_timestamp"],"WindSpeed":["WindSpeed (m/s)","wind_speed"],"DroneSpeed":["DroneSpeed (m/s)","drone_speed"],"PilotExperienced:":["PilotExperienced","pilot_experienced"],"Altitude":["Altitude (m)","altitude"],"Temperature":["Temperature (celcius)","temperature"],"Distance_from_remote_control":["Distance_from_remote_control (m)","distance_from_remote_control"],"Battery_level:":["Battery_level","battery_level"],"Low_visibility:":["Low_visibility","low_visibility"],"Normal_frame:":["Normal_frame","normal_frame"],"weather:":["weather","weather"],"upside_down:":["upside_down","upside_down"],"good_motor_condition:":["good_motor_condition","good_motor_condition"],"going_backwards:":["going_backwards","going_backwards"],"indoor:":["indoor","indoor"],"waterproof":["waterproof drone?","waterproof_drone"],"flying":["flying over","flying_over"],"criticality:":["criticality","criticality"],"Text1:":["Text1","text1"],"Text2:":["Text2","text2"]}

def make_record(row,id):
    action=[]
    text=row["Text2"].split("[SEP]\n")
    if len(text[0])==0:
      uttlist=""
      res={"dialogue_id": "003_{}".format("%05d" %id[0]),"services": ["Drone_1"], "turns": [{"frames": [{"actions": action,"service": "Drone_1","slots": []}],"speaker": "SYSTEM", "utterance": uttlist}]}
      return res
    elif text[0][0]=="(":
    	uttlist=text[0]
    	res={"dialogue_id": "003_{}".format("%05d" %id[0]),"services": ["Drone_1"], "turns": [{"frames": [{"actions": action,"service": "Drone_1","slots": []}],"speaker": "SYSTEM", "utterance": uttlist}]}
    else:
    	uttlist=text[:2]
    	res={"dialogue_id": "003_{}".format("%05d" %id[0]),"services": ["Drone_1"], "turns": [{"frames": [{"actions": action,"service": "Drone_1","slots": []}],"speaker": "SYSTEM", "utterance": "{}".format("   ".join(uttlist))}]}
    #print(tmp)
    #utterance
    
    #lostconnection
    if row["AC"]!="":
      action.append({"act": "INFORM","canonical_values": [ row["LostConnection"] ],"slot": "lost_connection", "values": [ row["LostConnection"] ]})
      tmp="".join([i for i in row["AC"] if not i.isdigit()])
      text1=row["AC"].split()
      #print(text1)
      for info in text1:
        for attrk,attrv in attribute.items():
          if attrk == info:
            action.append({"act": "INFORM","canonical_values": [ row[attrv[0]] ],"slot": attrv[1], "values": [ row[attrv[0]] ]})
    #risk of internal damage
    if row["Y"]!="":
      action.append({"act": "INFORM","canonical_values": [ row["RiskOfInternalDamage"] ],"slot": "risk_of_internal_damage", "values": [ row["RiskOfInternalDamage"] ]})
      text2=row["Y"].split()
      #print(text2)
      for info in text2:
        for attrk,attrv in attribute.items():
          if attrk == info:
            action.append({"act": "INFORM","canonical_values": [ row[attrv[0]] ],"slot": attrv[1], "values": [ row[attrv[0]] ]})
    #risk of physical damage
    starttime="00:99"
    #print(uttlist)
    count=1
    if row["W"]!="":
      action.append({"act": "INFORM","canonical_values": [ row["RiskOfPhysicalDamage"] ],"slot": "risk_of_physical_damage", "values": [ row["RiskOfPhysicalDamage"] ]})
      text3=row["W"].split(" OR ")
      text3new=[]
      timeshot=[]
      distanceshot=[]
      for i in text3:
        i.strip()
        if "at 00:" in i and i[-5:]!=starttime:
          count=count-1
          starttime=i[-5:]
          timeshot.append(starttime)
          #print(starttime)
        if count==-1:
          break
        text3new.append(i)

      #print(text3new)
      if len(timeshot)>0:
        timeshot.pop()
      #print(timeshot)
      #print(count)
      for i in text3new:
        textsub=i.split()
        count=0
        #print(textsub)
        for info in textsub:
          #print(info)
          count=count+1
          for attrk,attrv in attribute.items():
            if attrk == info:
              if attrk=="Type:" and textsub[count] == "Human":
                action.append({"act": "INFORM","canonical_values": [ row["RiskOfHumanDamage"] ],"slot": "risk_of_human_damage", "values": [ row["RiskOfHumanDamage"] ]})
              if attrk =="InPath:":
                action.append({"act": "INFORM","canonical_values": [ textsub[count] ],"slot": attrv[1], "values": [ textsub[count] ]})
              elif attrk =="Moving:":
                action.append({"act": "INFORM","canonical_values": [ textsub[count] ],"slot": attrv[1], "values": [ textsub[count] ]})
              elif attrk =="Type:":
                action.append({"act": "INFORM","canonical_values": [ textsub[count] ],"slot": attrv[1], "values": [ textsub[count] ]})
              elif attrk == "Distance:":
                action.append({"act": "INFORM","canonical_values": [ textsub[count] ],"slot": attrv[1], "values": [ textsub[count] ]})
                distanceshot.append(textsub[count])
              else:
                action.append({"act": "INFORM","canonical_values": [ row[attrv[0]] ],"slot": attrv[1], "values": [ row[attrv[0]] ]})
      timestamp=row["time_stamp"].strip("[]")
      timestamp1=timestamp.strip("datetime.time(0, ").rstrip(")").split("), datetime.time(0, ")
      #print(timestamp1)
      distancestamp=row["Distance"].strip("[]").split(", ")
      distancestamp1=[]
      for dis in distancestamp:
      	distancestamp1.append(dis.rstrip("0.")) 
      namestamp=row["name"].strip("[]'").split("', '")
      if len(timeshot)>0 and len(distanceshot)>0:
	      for i in range(len(timestamp1)):
	      	for j in range(len(distancestamp1)):
	      		if timestamp1[i]==timeshot[0].lstrip("0:") and distancestamp1[j]==distanceshot[0] and i==j:
	      			action.append({"act": "INFORM","canonical_values": [ namestamp[i] ],"slot": "object_name", "values": [ namestamp[i] ]})

    



    id[0]=id[0]+1
    return res




# dump to the json
id=[0]
with open(mcsv_path, 'r', newline='') as csvfile:
     reader = csv.DictReader(csvfile)
     with open(json_path, 'w') as jsonfile:    
        out = json.dumps([make_record(row,id) for row in reader], indent=2)
        jsonfile.write(out)



# issue/to be done:

# path to arg
# id, w
# time_stamp might have to reformat datetime.time(0,0,0) to 0:00