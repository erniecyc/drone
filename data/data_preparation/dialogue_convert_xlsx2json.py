import os, sys 
import csv, json
import pandas as pd
from pandas import DataFrame
import argparse

# path
parser = argparse.ArgumentParser()
parser.add_argument('--p1', help= 'xlsx')
parser.add_argument('--p2', help= 'merged.csv')
parser.add_argument('--p3', help= 'json')
args = parser.parse_args()

csv_path = args.p1
mcsv_path = args.p2
json_path = args.p3

# read csv file in xlsx
timestep = pd.read_excel(csv_path, 'timestep')
status = pd.read_excel(csv_path, 'status')
text = pd.read_excel(csv_path, 'text')


# timestep
timestep.fillna(method='ffill', inplace=True)   # fill Nan 
timestep_short = timestep.groupby(['Link'], as_index=False)[['name','ID_obj','Type','Moving','InPath','Distance','time_stamp']].agg(lambda x: list(x))   # group to list

# check
#print(timestep_short.head())
#print(len(timestep_short.index), len(status.index), len(text.index))
#print(timestep.columns)
#print(status.columns)
#print(text.columns)

# merge all csvs
merged = pd.merge(text, status, on=['Link'])
merged = pd.merge(merged, timestep_short, on=['Link'])
merged.to_csv(mcsv_path, index=False)
# print(len(merged.index), len(merged.columns))
# print(merged.columns)


# define json 
#1. only 1 atrrbute added to json, we need add all from three sheets?
#2. now they are all grouped by video, means texts and objects in differnt timestamp are in same json 
def make_record(row):
    return {
    "dialogue_id": 0,
    "services": [
      "drone"
    ],
    "turns": [
      {
        "frames": [
          {
            "actions": [
              {
                "act": "INFORM",
                "canonical_values": [
                  row["DroneSpeed (m/s)"]
                ],
                "slot": "dorne speed",
                "values": [
                  row["DroneSpeed (m/s)"]
                ]
              }
            ]
          }
        ],
        "speaker": "SYSTEM",
        "utterance": [row["Text1"]]
      }
    ]
  },




# dump to the json
with open(mcsv_path, 'r', newline='') as csvfile:
     reader = csv.DictReader(csvfile)
     with open(json_path, 'w') as jsonfile:    
        out = json.dumps([make_record(row) for row in reader], indent=2)
        jsonfile.write(out)



# issue/to be done:

# path to arg
# id, w
# time_stamp might have to reformat datetime.time(0,0,0) to 0:00
