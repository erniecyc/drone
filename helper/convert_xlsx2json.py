import os, sys 
import csv, json
import pandas as pd
from pandas import DataFrame
import argparse

# path
parser = argparse.ArgumentParser()
parser.add_argument('--p1', help= 'xlsx')
parser.add_argument('--p2', help= 'merged.csv')
parser.add_argument('--p3', help= 'json')
args = parser.parse_args()

csv_path = args.p1
mcsv_path = args.p2
json_path = args.p3

# read csv file in xlsx
timestep = pd.read_excel(csv_path, 'timestep')
status = pd.read_excel(csv_path, 'status')
text = pd.read_excel(csv_path, 'text')


# timestep
timestep.fillna(method='ffill', inplace=True)   # fill Nan 
timestep_short = timestep.groupby(['Link'], as_index=False)[['name','ID_obj','Type','Moving','InPath','Distance','time_stamp','obj_near','very close','reachable']].agg(lambda x: list(x))   # group to list

# check
# print(timestep_short.head())
# print(len(timestep_short.index), len(status.index), len(text.index))
# print(timestep.columns)
# print(status.columns)
# print(text.columns)

# merge all csvs
merged = pd.merge(text, status, on=['Link'])
merged = pd.merge(merged, timestep_short, on=['Link'])
merged.to_csv(mcsv_path, index=False)
# print(len(merged.index), len(merged.columns))
# print(merged.columns)


# define json 
def make_record(row):
    return {
    "id": 100,
    "text": [
       row["Text"]
    ],
    "kbs": {
      "W": [
        "criticality",
        row["criticality"],
        [ [
            "Link",
            row["Link"]
          ],
          [
            "WindSpeed (m/s)",
            row["WindSpeed (m/s)"]
          ],
          [
            "DroneSpeed (m/s)",
            row["DroneSpeed (m/s)"]
          ],
          [
            "PilotExperienced",
            row["PilotExperienced"]
          ],
          [
            "Altitude (m)",
            row["Altitude (m)"]
          ],
          [
            "Temperature (celcius)",
            row["Temperature (celcius)"]
          ],
          [
            "Distance_from_remote_control (m)",
            row["Distance_from_remote_control (m)"]
          ],
          [
            "Battery_level",
            row["Battery_level"]
          ],
          [
            "Low_visibility",
            row["Low_visibility "]
          ],
          [
            "Normal_frame",
            row["Normal_frame"]
          ],
          [
            "Low_visibility ",
            row["Low_visibility "]
          ],
          [
            "weather ",
            row["weather"]
          ],
          [
            "upside_down",
            row["upside_down"]
          ],
          [
            "good_motor_condition",
            row["good_motor_condition"]
          ],
          [
            "going_backwards",
            row["going_backwards"]
          ],
          [
            "indoor",
            row["indoor"]
          ],
          [
            "waterproof drone?",
            row["waterproof drone?"]
          ],
          [
            "flying over",
            row["flying over"]
          ],
          [
            "low altitude",
            row["low altitude"]
          ],
          [
            "fast speed",
            row["fast speed"]
          ],
          
          [
            "name",
            row["name"]
          ],
          [
            "ID_obj",
            row["ID_obj"]
          ],
          [
            "Type",
            row["Type"]
          ],

          [
            "Moving",
            row["Moving"]
          ],
          [
            "InPath",
            row["InPath"]
          ],
          [
            "Distance",
            row["Distance"]
          ],
          [
            "time_stamp",
            row["time_stamp"]
          ],
          [
            "obj_near",
            row["obj_near"]
          ],
          [
            "very close",
            row["very close"]
          ],
          [
            "reachable",
            row["reachable"]
          ]
          
        ]
      ]
    }
  }


# dump to the json
with open(mcsv_path, 'r', newline='') as csvfile:
     reader = csv.DictReader(csvfile)
     with open(json_path, 'w') as jsonfile:    
        out = json.dumps([make_record(row) for row in reader], indent=2)
        jsonfile.write(out)



# issue/to be done:

# path to arg
# id, w
# time_stamp might have to reformat datetime.time(0,0,0) to 0:00
