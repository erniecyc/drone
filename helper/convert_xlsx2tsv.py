import csv, json
from jsonschema import validate
import pandas as pd
from pandas import DataFrame
import argparse

# path
parser = argparse.ArgumentParser()
parser.add_argument('--p1', help= 'xlsx')
parser.add_argument('--p2', help= 'merged.csv')
parser.add_argument('--p3', help= 'tsv')
args = parser.parse_args()

csv_path = args.p1
mcsv_path = args.p2
tsv_path = args.p3

# read csv file 
timestep = pd.read_excel(csv_path, 'timestep')
status = pd.read_excel(csv_path, 'status')
text = pd.read_excel(csv_path, 'text')


# timestep
timestep.fillna(method='ffill', inplace=True)   # fill Nan 
timestep_short = timestep.groupby(['Link'], as_index=False)[['name','ID_obj','Type','Moving','InPath','Distance','time_stamp','obj_near','very close','reachable']].agg(lambda x: list(x))   # group to list

# check
# print(timestep_short.head())
# print(len(timestep_short.index), len(status.index), len(text.index))
# print(timestep.columns)
# print(status.columns)
# print(text.columns)

# merge all csvs
merged = pd.merge(text, status, on=['Link'])
merged = pd.merge(merged, timestep_short, on=['Link'])
merged.to_csv(mcsv_path, index=False)
# print(len(merged.index), len(merged.columns))
# print(merged.columns)


# dump to tsv
with open(mcsv_path,'r') as csvin, open(tsv_path, 'w') as tsvout:
    csvin = csv.reader(csvin)
    tsvout = csv.writer(tsvout, delimiter='\t')
    for row in csvin:
        tsvout.writerow(row)


# issue/to be done:
# path to arg
# id, w
# time_stamp might have to reformat datetime.time(0,0,0) to 0:00
