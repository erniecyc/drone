import nltk
from nltk import word_tokenize
from nltk.collocations import BigramCollocationFinder
import matplotlib.pylab as plt
import numpy as np

# path = '...'
# text = f.read(open(path))
text = "Text messaging, or texting, is the act of composing and sending electronic messages, typically consisting of alphabetic and numeric characters, between two or more users of mobile devices, desktops/laptops, or other type of compatible computer. Text messages may be sent over a cellular network, or may also be sent via an Internet connection."
tokens = nltk.word_tokenize(text)

# create n-grams
bi_gram = nltk.bigrams(tokens)
tri_gram = nltk.trigrams(tokens)

# compute frequency distribution & filter top 20
fdist = nltk.FreqDist(bi_gram)
flists= sorted(fdist.items(), key=lambda item: item[1], reverse=True)
flists = flists[0:20]

# for obj in flists:
#     obj = obj[0][0] + ' ' + obj[0][1]
# print(flists)


for i,item in enumerate(flists):
    item = list(item)
    item[0] = item[0][0] + ' ' + item[0][1]
    item = tuple(item)
    flists[i] = item
    # print(item)
# print(flists)

# plot
plt.style.use('ggplot')

fig, ax = plt.subplots(1, figsize=(50,50))
ax.set_xlabel('Bigrams')
ax.set_ylabel('Frequency')

labels, ys = zip(*flists)
xs = np.arange(len(labels)) 
width = 0.8
ax.bar(xs, ys, width, align='center', color='dimgray')
plt.xticks(fontsize=8,rotation=80)
plt.xticks(xs, labels)


plt.show()





