### Drone


#### Reset
```
rm -rf checkpoint_dronenlg
rm decoded_results.txt
```

#### Download Pre-trained KGPT Model
```
wget https://kgpt.s3-us-west-2.amazonaws.com/models.zip
unzip models.zip
```

#### Download Dataset

drone
```
mkdir text_annotation
<manually put the xlsx to the folder: text_annotation>
```

#### Pre-processing

convert xlsx to json
```
python ~/helper/convert_xlsx2json.py --p1 ~/text_annotation/random.xlsx --p2 ~/text_annotation/merged.csv --p3 ~/dataset/dronenlg/train.json
```

convert xlsx to tsv
```
python ~/helper/convert_xlsx2tsv.py --p1 ~/text_annotation/random.xlsx --p2 ~/text_annotation/merged.csv --p3 ~/dataset/dronenlg/train.tsv
```

train-val-test split
```
<prepare train.json, val.json, test.json in ~/dataset/dronenlg>
```

#### Finetune for Few-Shot Leanring
```
 bash scripts/dronenlg/finetune_sequence_dronenlg_from_wikidata.sh 0 checkpoint_wikidata/checkpoint_sequence_head8_layer6_GPT2_maxfact12/model_ep14.pt 0.01
```

#### Model Selection
```
bash scripts/dronenlg/eval_sequence_dronenlg_all.sh 0 test checkpoint_dronenlg/checkpoint_finetune_sequence_head8_layer6_GPT2_maxfact12
```

#### Test
```
bash scripts/dronenlg/eval_sequence_dronenlg.sh 7 test checkpoint_dronenlg/checkpoint_finetune_sequence_head8_layer6_GPT2_maxfact12/model_ep12.pt
```
